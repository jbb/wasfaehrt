// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include <KLocalizedContext>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>

#include <chrono>

#include "departuremodel.h"
#include "weathermodel.h"
#include "upcomingeventsmodel.h"
#include "stwberlin.h"

#include "config.h"

#include "iconimageprovider.h"

using namespace std::chrono_literals;

constexpr auto URI = "ga.ghsq.jbb.wasfahert";

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationDomain(QStringLiteral("ghsq.ga"));
    QCoreApplication::setApplicationName(QStringLiteral("wasfahert"));

    QQmlApplicationEngine engine;

    // Populate icon theme search paths ourselves, so we can run independently of x11 / wayland
    if (QIcon::themeSearchPaths().empty() || QIcon::themeSearchPaths() == QStringList {QStringLiteral(":/icons")}) {
        QIcon::setThemeSearchPaths({QStringLiteral("/usr/share/icons/")});
    }
    QIcon::setThemeName(QStringLiteral("breeze"));

    qmlRegisterSingletonType<DepartureModel>(URI, 1, 0, "DepartureModel", [](QQmlEngine *engine, QJSEngine *) {
        return new DepartureModel({Config::transportBackends(), Config::nearbyStations(), 30s}, engine);
    });
    qmlRegisterSingletonType<WeatherForecast>(URI, 1, 0, "WeatherForecast", [](QQmlEngine *engine, QJSEngine *) {
        return new WeatherForecast(Config::weatherLocation(), 1h, engine);
    });
    qmlRegisterSingletonType<UpcomingEventsModel>(URI, 1, 0, "UpcomingEventsModel", [](QQmlEngine *engine, QJSEngine *) {
        return new UpcomingEventsModel(Config::calendarUrls(), 30s, engine);
    });
    qmlRegisterSingletonType<STWBerlinMenuModel>(URI, 1, 0, "STWBerlinMenuModel", [](QQmlEngine *, QJSEngine *) {
        return new STWBerlinMenuModel(Config::mensaId());
    });
    qmlRegisterType<QSortFilterProxyModel>(URI, 1, 0, "FilterModel");
    qmlRegisterSingletonInstance<Config>(URI, 1, 0, "Config", Config::self());
    engine.addImageProvider("icon", new IconImageProvider());

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().empty()) {
        return -1;
    }

    return app.exec();
}
