// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

import ga.ghsq.jbb.wasfahert 1.0

Controls.ApplicationWindow {
    id: window

    visible: true
    width: 800
    height: 700

    visibility: Controls.ApplicationWindow.FullScreen

    readonly property int headerSize: 15 * Config.scaleFactor
    readonly property int textSize: 10 * Config.scaleFactor

    property date currentDateTime: new Date

    Timer {
        interval: 100
        running: true
        repeat: true
        onTriggered: currentDateTime = new Date
    }

    header: Controls.Control {
        background: Rectangle{
            color: "#dcdcdc"
        }

        implicitHeight: headerLayout.implicitHeight

        RowLayout {
            id: headerLayout
            anchors {
                right: parent.right
                left: parent.left
            }

            Controls.Label {
                Layout.fillWidth: true
                Layout.margins: 10
                font.pixelSize: headerSize
                text: Qt.formatDate(currentDateTime)
            }
            Controls.BusyIndicator {
                visible: DepartureModel.loading
                         || WeatherForecast.loading
                         || UpcomingEventsModel.loading
                         || STWBerlinMenuModel.loading
                Layout.preferredHeight: headerSize + 10 * 2
            }

            Controls.Label {
                font.pixelSize: headerSize
                text: Qt.formatTime(currentDateTime)
                Layout.margins: 10
            }
        }
    }

    Timer {
        interval: 20000

        running: Config.mensaId
        repeat: true

        onTriggered: {
            console.log("Switching view from", swipeView.currentIndex)
            swipeView.currentIndex = (swipeView.currentIndex + 1) % swipeView.count
        }
    }

    Controls.SwipeView {
        id: swipeView

        anchors.fill: parent

        currentIndex: 0

        RowLayout {
            anchors.topMargin: 10

            // Departures
            ListView {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.preferredWidth: (window.width / 5) * 3

                clip: true

                interactive: false
                model: DepartureModel
                delegate: Controls.ItemDelegate {
                    width: parent ? parent.width : 0
                    height: contentLayout.height + 20
                    id: item

                    required property string line
                    required property date arrivalTime
                    required property string destination
                    required property color lineColour
                    required property int arrivalDelay
                    required property string station

                    RowLayout {
                        id: contentLayout
                        anchors.margins: 10
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter

                        ColumnLayout {
                            Layout.fillHeight: true
                            Layout.fillWidth: true

                            RowLayout {
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                // Line symbol
                                Item {
                                    implicitWidth: 30 * Config.scaleFactor
                                    implicitHeight: box.implicitHeight

                                    Item {
                                        id: box

                                        implicitWidth: lineLabel.implicitWidth + 5 * Config.scaleFactor
                                        implicitHeight: lineLabel.implicitHeight + 5 * Config.scaleFactor

                                        Controls.Label {
                                            id: lineLabel
                                            anchors.centerIn: parent
                                            text: item.line
                                            font.pixelSize: textSize
                                            color: "white"
                                        }
                                        Rectangle {
                                            radius: 3 * Config.scaleFactor
                                            color: lineColour
                                            anchors.fill: parent
                                            z: -1
                                        }
                                    }
                                }

                                ColumnLayout {
                                    // Destination
                                    Controls.Label {
                                        font.pixelSize: textSize
                                        Layout.fillHeight: true
                                        text: item.destination
                                    }

                                    RowLayout {
                                        Layout.fillWidth: true

                                        Controls.Label {
                                            font.pixelSize: textSize
                                            font.bold: true
                                            text: Qt.formatTime(item.arrivalTime)
                                        }
                                        Controls.Label {
                                            font.pixelSize: textSize
                                            color: "red"
                                            font.bold: true
                                            visible: item.arrivalDelay != 0
                                            text: item.arrivalDelay > 0 ? "+%1".arg(item.arrivalDelay) : "%1".arg(item.arrivalDelay)
                                        }
                                    }
                                }
                            }
                        }
                        Item {
                            Layout.fillWidth: true
                        }
                        Controls.Label {
                            Layout.fillHeight: true
                            font.pixelSize: textSize
                            text: qsTr("from %1").arg(item.station)
                        }
                    }
                }
            }

            ColumnLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true

                // Weather
                RowLayout {
                    Layout.fillWidth: true
                    Layout.margins: 10 * Config.scaleFactor

                    Image {
                        Layout.alignment: Qt.AlignHCenter

                        width: 100 * Config.scaleFactor
                        height: 100 * Config.scaleFactor
                        sourceSize: Qt.size(width, height)
                        source: "image://icon/" + WeatherForecast.iconName
                    }

                    ColumnLayout {
                        Controls.Label {
                            font.pixelSize: textSize
                            font.bold: true
                            text: qsTr("Temperature")
                        }
                        Controls.Label {
                            font.pixelSize: textSize
                            text: "%1 °C".arg(WeatherForecast.temperature)
                        }

                        Controls.Label {
                            font.pixelSize: textSize
                            font.bold: true
                            text: qsTr("Wind speed")
                        }
                        Controls.Label {
                            font.pixelSize: textSize
                            text: qsTr("%1 km/h").arg(WeatherForecast.windSpeed)
                        }

                        Controls.Label {
                            font.pixelSize: textSize
                            font.bold: true
                            text: qsTr("Humidity")
                        }
                        Controls.Label {
                            font.pixelSize: textSize
                            text: qsTr("%1%").arg(WeatherForecast.humidity)
                        }
                    }
                }

                // Events
                ColumnLayout {
                    Layout.fillWidth: true
                    Controls.Label {
                        Layout.alignment: Qt.AlignHCenter
                        font.pixelSize: headerSize
                        text: qsTr("Events Today")
                    }

                    ListView {
                        Layout.topMargin: 10 * Config.scaleFactor
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.margins: 50

                        interactive: false

                        model: UpcomingEventsModel
                        delegate: Controls.ItemDelegate {
                            id: eventItem
                            implicitHeight: eventLayout.implicitHeight + 5 * Config.scaleFactor
                            implicitWidth: eventLayout.implicitWidth + 5 * Config.scaleFactor
                            width: parent.width

                            required property date dateTime
                            required property string description
                            required property string title
                            required property color colour

                            RowLayout {
                                id: eventLayout
                                width: parent.width

                                Controls.Label {
                                    font.pixelSize: textSize
                                    Layout.margins: 10 * Config.scaleFactor
                                    text: Qt.formatTime(eventItem.dateTime)
                                }
                                Controls.Control {
                                    Layout.fillWidth: true
                                    implicitHeight: eventContentLayout.implicitHeight + 20 * Config.scaleFactor

                                    background: Item {
                                        Rectangle {
                                            anchors.fill: parent
                                            id: back
                                            color: eventItem.colour
                                            opacity: 0.3
                                            radius: 5 * Config.scaleFactor
                                        }

                                        Rectangle {
                                            color: eventItem.colour
                                            width: 5 * Config.scaleFactor
                                            opacity: 1
                                            anchors {
                                                left: back.left
                                                top: back.top
                                                bottom: back.bottom
                                            }
                                        }
                                    }

                                    ColumnLayout {
                                        id: eventContentLayout
                                        anchors {
                                            fill: parent
                                            margins: 10 * Config.scaleFactor
                                        }

                                        Controls.Label {
                                            Layout.fillWidth: true
                                            font.pixelSize: textSize
                                            text: eventItem.title
                                            elide: Qt.ElideRight
                                        }

                                        Controls.Label {
                                            Layout.fillWidth: true
                                            font.pixelSize: textSize * 0.5
                                            text: eventItem.description
                                            wrapMode: Text.WordWrap
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Loader {
            active: Config.mensaId

            sourceComponent: RowLayout {
                ListView {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.preferredHeight: 500

                    model: FilterModel {
                        id: mensaModel
                        sourceModel: STWBerlinMenuModel

                        filterRole: STWBerlinMenuModel.Category
                        filterRegExp: /Essen|Aktionen/
                    }

                    delegate: Controls.ItemDelegate {
                        id: mensaDelegate
                        required property string meal
                        required property string priceStudent
                        required property string priceEmployee
                        required property string priceExternal
                        required property var icons
                        required property string category
                        required property int index

                        implicitHeight: 40* Config.scaleFactor

                        onIconsChanged: console.log(icons)

                        width: parent.width
                        text: meal

                        contentItem: RowLayout {
                            Controls.Label {
                                Layout.fillWidth: true
                                text: meal
                                font.pixelSize: textSize
                            }

                            Repeater {
                                model: Array.from(mensaDelegate.icons)
                                required property string modelData

                                delegate: Image {
                                    source: modelData

                                    sourceSize.height: 20 * Config.scaleFactor
                                }
                            }

                            Item {
                                implicitWidth: 20 * Config.scaleFactor
                            }

                            Controls.Label {
                                text: "%1€".arg(priceStudent)
                                textFormat: Text.StyledText
                                font.pixelSize: textSize
                            }
                        }
                    }
                }
            }
        }
    }
}
