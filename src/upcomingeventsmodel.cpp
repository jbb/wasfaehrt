// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "upcomingeventsmodel.h"

#include <QNetworkReply>
#include <QSharedPointer>

#include <KCalendarCore/ICalFormat>

#include <utility>

namespace ranges = std::ranges;

UpcomingEventsModel::UpcomingEventsModel(QList<QUrl> calendarUrl, std::chrono::duration<int> refreshInterval, QObject *parent)
    : QAbstractListModel(parent)
    , m_calendarUrls(std::move(calendarUrl))
    , m_loading(false)
{
    loadData();
    m_refreshTimer.setSingleShot(false);
    m_refreshTimer.setInterval(refreshInterval);
    m_refreshTimer.callOnTimeout(this, &UpcomingEventsModel::loadData);
    m_refreshTimer.start();
}

QHash<int, QByteArray> UpcomingEventsModel::roleNames() const
{
    return {
        { Description, "description" },
        { DateTime, "dateTime" },
        { Title, "title" },
        { Colour, "colour" }
    };
}

QVariant UpcomingEventsModel::data(const QModelIndex &index, int role) const
{
    const auto &event = m_events.at(index.row());

    switch (role) {
    case Description:
        return event.description;
    case DateTime:
        return event.time;
    case Title:
        return event.title;
    case Colour:
        return event.colour;
    }

    Q_UNREACHABLE();
}

int UpcomingEventsModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_events.size();
}

void UpcomingEventsModel::loadData()
{
    if (m_calendarUrls.isEmpty()) {
        return;
    }

    setLoading(true);

    m_events.clear();

    for (const auto &url : m_calendarUrls) {
        qDebug() << "Fetching new Calendar data for" << url;
        auto reply = m_nam.get(QNetworkRequest(url));
        connect(reply, &QNetworkReply::finished, this, [this, reply]{
            KCalendarCore::ICalFormat ical;
            auto calendar = QSharedPointer<KCalendarCore::MemoryCalendar>(
                        new KCalendarCore::MemoryCalendar(QTimeZone::systemTimeZone()));
            bool success = ical.fromRawString(calendar, reply->readAll());

            Q_EMIT colourChanged();

            if (!success) {
                qDebug() << "Failed to parse Calendar.";
            }

            reply->deleteLater();

            qDebug() << "Imported" << calendar->events().size() << "events";

            const auto calEvents = calendar->events(QDateTime::currentDateTime().date(), {}, KCalendarCore::EventSortStartDate);

            beginResetModel();
            ranges::transform(calEvents, std::back_inserter(m_events), [&](const KCalendarCore::Event::Ptr &event) {
                return Event {
                    .description = event->description(),
                    .title = event->summary(),
                    .time  = event->dateTime(KCalendarCore::Event::DateTimeRole::RoleDisplayStart),
                    .colour = calendar->nonKDECustomProperty("X-APPLE-CALENDAR-COLOR"),
                };
            });
            ranges::sort(m_events, [](const Event &a, const Event &b) {
                return a.time > b.time;
            });
            endResetModel();

            setLoading(false);
        });
    }
}

bool UpcomingEventsModel::loading() const {
    return m_loading;
}

void UpcomingEventsModel::setLoading(bool loading) {
    m_loading = loading;
    Q_EMIT loadingChanged();
}
