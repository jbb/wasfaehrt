// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "iconimageprovider.h"

#include <QIcon>
#include <QDebug>

IconImageProvider::IconImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Pixmap)
{
}

QPixmap IconImageProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    auto pixmap = QIcon::fromTheme(id).pixmap(requestedSize);
    *size = pixmap.size();
    return pixmap;
}
