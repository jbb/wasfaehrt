// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <QAbstractListModel>
#include <QNetworkAccessManager>
#include <QTimer>

#include <KCalendarCore/MemoryCalendar>

#include <chrono>

struct Event {
    QString description;
    QString title;
    QDateTime time;
    QColor colour;
};

class UpcomingEventsModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)

    enum Role {
        Description,
        DateTime,
        Title,
        Colour
    };

public:
    explicit UpcomingEventsModel(QList<QUrl> calendarUrl, std::chrono::duration<int> refreshInterval, QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;

    QColor colour() const;
    Q_SIGNAL void colourChanged();

    bool loading() const;
    void setLoading(bool loading);
    Q_SIGNAL void loadingChanged();

private:
    void loadData();

    std::vector<Event> m_events;
    QNetworkAccessManager m_nam;
    QList<QUrl> m_calendarUrls;
    QTimer m_refreshTimer;

    bool m_loading;
};

