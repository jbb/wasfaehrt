// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <QAbstractListModel>
#include <QNetworkAccessManager>
#include <QTimer>

#include <Desert/Reflection>
#include <Desert/Json>

using namespace desert;

struct Icon {
    DESERT_OBJECT

    Attribute<u"high", QString> highResPath;
    Attribute<u"low", QString> lowResPath;
};

struct Meal {
    DESERT_OBJECT

    Attribute<u"title_clean", QString> title;
    Attribute<u"category", QString> category;
    Attribute<u"co2_bewertung", QString> co2Rating;
    Attribute<u"h2o_bewertung", QString> h2oRating;
    Attribute<u"preis1_eur", QString> priceStudents;
    Attribute<u"preis2_eur", QString> priceEmployees;
    Attribute<u"preis3_eur", QString> priceExternal;
    Attribute<u"icons2", QHash<QString, Icon>> icons;
};

struct MensaDay {
    DESERT_OBJECT

    Attribute<u"timestamp", qint64> timestampSecs;
};

struct MensaResult {
    DESERT_OBJECT

    Attribute<u"essen", std::vector<Meal>> meals;
    Attribute<u"tag", MensaDay> day {};
};

struct MensaResponse {
    DESERT_OBJECT

    Attribute<u"mensaname", QString> mensaName;
    Attribute<u"result", std::vector<MensaResult>> result;
};

class STWBerlinMenuModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString mensaName READ mensaName NOTIFY mensaNameChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)

public:
    STWBerlinMenuModel(QString mensaId);

    enum Role {
        Meal = Qt::UserRole + 1,
        Category,
        PriceStudent,
        PriceEmployee,
        PriceExternal,
        Icons
    };
    Q_ENUM(Role);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;

    void loadMenu();

    QString mensaName() const;
    Q_SIGNAL void mensaNameChanged();

    bool loading();
    void setLoading(bool loading);
    Q_SIGNAL void loadingChanged();

private:
    QString m_mensaId;
    QNetworkAccessManager m_nm;
    MensaResponse m_resp;
    QTimer m_refreshTimer;
    bool m_loading = false;
};
