// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <QObject>
#include <QQuickImageProvider>
#include <QPixmap>

class IconImageProvider : public QQuickImageProvider
{
public:
    explicit IconImageProvider();

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) override;

};

