// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "departuremodel.h"

#include <QDateTime>
#include <QTimer>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QColor>

#include <KPublicTransport/StopoverRequest>
#include <KPublicTransport/LocationRequest>
#include <KPublicTransport/StopoverReply>
#include <KPublicTransport/LocationReply>
#include <KPublicTransport/Location>
#include <KPublicTransport/Backend>

#include <ranges>

namespace ranges = std::ranges;

DepartureModel::DepartureModel(const DepartureModelOptions &&options, QObject *parent)
    : QAbstractListModel(parent)
    , m_manager(std::make_unique<KPublicTransport::Manager>())
{
    // Set up backends
    m_manager->setBackendsEnabledByDefault(false);
    m_manager->setEnabledBackends(options.enabledBackends);

    for (const auto &location : std::as_const(options.locations)) {
        // Resolve the location
        KPublicTransport::Location loc;
        loc.setName(location);
        requestStarted();
        auto locationReply = m_manager->queryLocation(loc);

        connect(locationReply, &KPublicTransport::Reply::finished, this, [this, locationReply, location] {
            requestFinished();
            if (locationReply->result().empty()) {
                qFatal("Failed to resolve station location %s", qPrintable(location));
            }
            addLocation(std::move(locationReply->takeResult().front()));
            locationReply->deleteLater();
            if (!loading()) {
                loadData();
            }
        });
    }

    // Set up refreshing
    m_refreshTimer.setInterval(options.refreshInterval);
    m_refreshTimer.setSingleShot(false);
    m_refreshTimer.callOnTimeout(this, &DepartureModel::loadData);
    m_refreshTimer.start();
}

QHash<int, QByteArray> DepartureModel::roleNames() const {
    return {
        { Line, "line" },
        { ArrivalTime, "arrivalTime" },
        { Destination, "destination" },
        { LineColour, "lineColour" },
        { ArrivalDelay, "arrivalDelay" },
        { Station, "station" }
    };
}

QVariant DepartureModel::data(const QModelIndex &index, int role) const {
    switch (role) {
    case Destination:
        return m_contents[index.row()].route().direction();
    case ArrivalTime:
        return m_contents[index.row()].scheduledArrivalTime();
    case Line:
        return m_contents[index.row()].route().line().name();
    case LineColour:
        return m_contents[index.row()].route().line().color();
    case ArrivalDelay:
        return m_contents[index.row()].arrivalDelay();
    case Station:
        return m_contents[index.row()].stopPoint().name();
    }

    Q_UNREACHABLE();
}

int DepartureModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : int(m_contents.size());
}

void DepartureModel::loadData()
{
    qDebug() << "Fetching new departures…";
    beginResetModel();
    m_contents.clear();

    for (const auto &location : std::as_const(m_locations)) {
        requestStarted();

        KPublicTransport::StopoverRequest req;
        req.setStop(location);
        req.setMode(KPublicTransport::StopoverRequest::Mode::QueryArrival);

        const auto reply = m_manager->queryStopover(req);
        connect(reply, &KPublicTransport::Reply::finished, this, [this, reply]() {
            requestFinished();
            m_contents.reserve(m_contents.size() + reply->result().size());
            qDebug() << "Received" << reply->result().size() << "entries";

            for (auto &&item : reply->takeResult()) {
                m_contents.push_back(std::move(item));
            }

            reply->deleteLater();

            // All requests finished
            if (!loading()) {
                ranges::sort(m_contents,
                             [](const KPublicTransport::Stopover &a, const KPublicTransport::Stopover &b) {

                    if (a.expectedArrivalTime().isValid() && b.expectedArrivalTime().isValid()) {
                        return a.expectedArrivalTime() < b.expectedArrivalTime();
                    }

                    return a.scheduledArrivalTime() < b.scheduledArrivalTime();
                });
                endResetModel();
            }
        });
    }
}

QString DepartureModel::location() const
{
    if (m_locations.empty()) {
        return {};
    }

    QString out;
    std::for_each(m_locations.begin(), m_locations.end() - 1, [&out](const auto &location) {
        out.append(location.name());
        out.append(", ");
    });
    out.append(m_locations.back().name());
    return out;
}

void DepartureModel::addLocation(KPublicTransport::Location &&location)
{
    m_locations.push_back(std::move(location));
    Q_EMIT locationChanged();
}

void DepartureModel::requestFinished()
{
    m_runningRequests--;
    if (m_runningRequests < 1) {
        Q_EMIT loadingChanged();
    }
}

void DepartureModel::requestStarted()
{
    m_runningRequests++;
    if (m_runningRequests == 1) {
        Q_EMIT loadingChanged();
    }
}

bool DepartureModel::loading() const {
    return m_runningRequests != 0;
}
