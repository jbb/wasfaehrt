// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "weathermodel.h"

#include <KWeatherCore/LocationQuery>
#include <KWeatherCore/LocationQueryReply>

WeatherForecast::WeatherForecast(const QString &location, std::chrono::duration<int> refreshInterval, QObject *parent)
    : QObject(parent)
    , m_loading(true)
{
    auto *query = new KWeatherCore::LocationQuery(this);
    auto reply = query->query(location);
    connect(reply, &KWeatherCore::Reply::finished, this, [this, reply]() {
        if (reply->result().empty()) {
            qWarning() << "Failed to resolve weather location";
            if (reply->error() != KWeatherCore::Reply::NoError) {
                qWarning() << reply->errorMessage();
            }
            return;
        };
        m_location = reply->result().front();

        reply->deleteLater();

        loadData();
    });

    // Set up refreshing
    m_refreshTimer.setInterval(refreshInterval);
    m_refreshTimer.setSingleShot(false);
    m_refreshTimer.callOnTimeout(this, &WeatherForecast::loadData);
    m_refreshTimer.start();
}

QDateTime WeatherForecast::time() const
{
    return m_forecast.date();
}

QString WeatherForecast::iconName() const
{
    return m_forecast.weatherIcon();
}

double WeatherForecast::temperature() const
{
    return m_forecast.temperature();
}

double WeatherForecast::windSpeed() const
{
    return m_forecast.windSpeed();
}

double WeatherForecast::humidity() const
{
    return m_forecast.humidity();
}

void WeatherForecast::loadData()
{
    qDebug() << "Fetching new weather forecast for" << m_location.latitude() << m_location.longitude();
    auto pending = m_source.requestData(m_location.latitude(), m_location.longitude());

    auto loadReceivedData = [=, this] {
        m_forecast = pending->value().dailyWeatherForecast().front().hourlyWeatherForecast().front();
        qDebug() << "Received forecast";
        pending->deleteLater();
        Q_EMIT forecastChanged();
        setLoading(false);
    };

    connect(pending, &KWeatherCore::PendingWeatherForecast::finished, this, loadReceivedData);

    connect(pending, &KWeatherCore::PendingWeatherForecast::finished, this, [this, pending, loadReceivedData] {
        if (pending->error() != KWeatherCore::Reply::NoError) {
            qDebug() << "Network error while fetching weather forecast";
            pending->deleteLater();
            setLoading(false);
        } else {
            loadReceivedData();
        }
    });
}

bool WeatherForecast::loading() const {
    return m_loading;
}

void WeatherForecast::setLoading(bool loading)
{
    m_loading = loading;
    Q_EMIT loadingChanged();
}
