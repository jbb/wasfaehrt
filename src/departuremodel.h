// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <QObject>
#include <QAbstractListModel>
#include <QTimer>
#include <QStringList>

#include <KPublicTransport/Manager>
#include <KPublicTransport/Stopover>

#include <memory>
#include <chrono>

struct DepartureModelOptions {
    const QStringList enabledBackends;
    const QStringList locations;
    std::chrono::duration<int> refreshInterval;
};

class DepartureModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QString location READ location NOTIFY locationChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)

    enum Role {
        Line = Qt::UserRole + 1,
        LineColour,
        ArrivalTime,
        Destination,
        ArrivalDelay,
        Station
    };

public:
    explicit DepartureModel(const DepartureModelOptions &&options, QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &parent, int role) const override;
    int rowCount(const QModelIndex &parent) const override;

    void loadData();

    QString location() const;
    void addLocation(KPublicTransport::Location &&location);
    Q_SIGNAL void locationChanged();

    bool loading() const;
    void requestFinished();
    void requestStarted();
    Q_SIGNAL void loadingChanged();

private:
    std::unique_ptr<KPublicTransport::Manager> m_manager;
    std::vector<KPublicTransport::Stopover> m_contents;
    std::vector<KPublicTransport::Location> m_locations;
    QTimer m_refreshTimer;
    int m_runningRequests = 0;
};

