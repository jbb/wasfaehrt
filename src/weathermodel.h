// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <QObject>
#include <QTimer>
#include <KWeatherCore/WeatherForecastSource>
#include <KWeatherCore/LocationQueryResult>

#include <chrono>

class WeatherForecast : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString iconName READ iconName NOTIFY forecastChanged)
    Q_PROPERTY(double temperature READ temperature NOTIFY forecastChanged)
    Q_PROPERTY(float windSpeed READ windSpeed NOTIFY forecastChanged)
    Q_PROPERTY(double humidity READ humidity NOTIFY forecastChanged)
    Q_PROPERTY(QDateTime time READ time NOTIFY forecastChanged)

    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)

public:
    explicit WeatherForecast(const QString &location, std::chrono::duration<int> refreshInterval, QObject *parent = nullptr);

    QDateTime time() const;
    QString iconName() const;
    double temperature() const;
    double windSpeed() const;
    double humidity() const;

    Q_SIGNAL void forecastChanged();

    bool loading() const;
    void setLoading(bool loading);
    Q_SIGNAL void loadingChanged();

private:
    void loadData();

    KWeatherCore::LocationQueryResult m_location;
    KWeatherCore::WeatherForecastSource m_source;
    KWeatherCore::HourlyWeatherForecast m_forecast;

    QTimer m_refreshTimer;
    bool m_loading;
};
