// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "stwberlin.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStringBuilder>

#include <utility>
#include <ranges>

using namespace std::literals;

STWBerlinMenuModel::STWBerlinMenuModel(QString mensaId)
    : QAbstractListModel()
    , m_mensaId(std::move(mensaId))
{
    m_refreshTimer.setInterval(1h);
    m_refreshTimer.setSingleShot(false);
    m_refreshTimer.callOnTimeout(this, &STWBerlinMenuModel::loadMenu);
    m_refreshTimer.start();

    loadMenu();
}

QHash<int, QByteArray> STWBerlinMenuModel::roleNames() const
{
    return {
        { Role::Meal, "meal" },
        { Role::Category, "category" },
        { Role::PriceStudent, "priceStudent" },
        { Role::PriceEmployee, "priceEmployee" },
        { Role::PriceExternal, "priceExternal" },
        { Role::Icons, "icons" },
    };
}

QVariant STWBerlinMenuModel::data(const QModelIndex &index, int role) const
{
    const auto &entry = m_resp.result->front().meals->at(index.row());
    switch (role) {
    case Role::Meal:
        return *entry.title;
    case Role::Category:
        return *entry.category;
    case Role::PriceStudent:
        return *entry.priceStudents;
    case Role::PriceEmployee:
        return *entry.priceEmployees;
    case Role::PriceExternal:
        return *entry.priceExternal;
    case Role::Icons:
        const auto icons = entry.icons->values();
        std::vector<QUrl> urls;
        std::ranges::transform(icons, std::back_inserter(urls), [](const Icon &icon) {
            QUrl url("https://app2022.stw.berlin/");
            url.setPath(*icon.highResPath);
            return url;
        });
        return QVariant::fromValue(urls);
    }

    return {};
}

int STWBerlinMenuModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_resp.result->empty() ? 0 : m_resp.result->front().meals->size();
}

void STWBerlinMenuModel::loadMenu()
{
    if (m_mensaId.isEmpty()) {
        return;
    }

    setLoading(true);

    const QUrl url("https://app2022.stw.berlin/api/getdata.php?mensa_id="
        % m_mensaId % "&json=1&hyp=1&now="
        % QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch())
        % "&mode=slsys&lang=de");

    auto reply = m_nm.get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, this, [reply, this]() {
        auto json = reply->readAll();
        auto data = desert::json::deserialize<MensaResponse>(json);

        beginResetModel();
        m_resp = std::move(data);
        endResetModel();

        reply->deleteLater();
        setLoading(false);
    });
}

QString STWBerlinMenuModel::mensaName() const {
    return *m_resp.mensaName;
}

bool STWBerlinMenuModel::loading()
{
    return m_loading;
}

void STWBerlinMenuModel::setLoading(bool loading)
{
    if (m_loading == loading) {
        return;
    }

    m_loading = loading;
    Q_EMIT loadingChanged();
}
