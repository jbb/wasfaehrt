# Departure Monitor

![Build status](https://ci.jbb.ghsq.de/api/badges/jbb/wasfaehrt/status.svg)

![Screenshot](screenshot.png)

A simple fullscreen application that displays public transport departures in the area, the current weather and upcoming events.
It can be used at home, in your hackerspace or office, or at your train station, if you happen to operate one :D

## Building (for development)

Optional: Create a debian container if your distribution does not package all of the needed dependencies:
```
podman build . -t wasfaehrt-buildenv
distrobox create --image wasfaehrt-buildenv --name wasfaehrt
distrobox enter wasfaehrt
```

Then, proceed to building:
```
cd <source directory>
mkdir -p build
cmake -B build
cmake --build build -j$(nproc)
```

Optionally install QtCreator inside the distrobox in order to have a suitable IDE.

Finally, run it:
```
./build/bin/wasfaehrt
```

## Config file

The application itself is non-interactive. A config file is used to set up the data sources to display.
The config file needs to be placed in `~/.config/departuresrc`

```toml
[General]
weatherLocation = Berlin
calendarUrls = https://path/to/ical/calendar
transportBackends = de_bb_vbb, de_bb_bvg
nearbyStations = Berlin Hauptbahnhof, Berlin Südkreuz
scaleFactor = 2.8
```

## License

The project is licensed under AGPL-3.0-only
